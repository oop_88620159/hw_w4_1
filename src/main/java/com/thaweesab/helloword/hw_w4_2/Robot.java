package com.thaweesab.helloword.hw_w4_2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N ;
    private char lastDirection = 'N';
    
    public Robot(int x,int y,int bx,int by,int N){
        this.N = N ;
        if(!inMap(x,y)){
            this.x = 0;
            this.y = 0;
        } else{
            this.x = x;
            this.y = y;
        } if(!inMap(bx,by)){
            this.bx = bx;
            this.by = by;
        } else {
            this.bx = bx;
            this.by = by;
        }
        
    }
    public boolean walk(char direction){
       if(direction=='N'){
          if(!inMap(x,y-1)){
              printCM();
              return false;
          }
           y-=1;
       } else if (direction=='S'){
           if(!inMap(x,y+1)){
              printCM();
               return false;
          }
           y+=1;
       } else if(direction == 'E'){
           if(!inMap(x+1,y)){
              printCM();
               return false;
          }
           x+=1;
       } else if(direction == 'W'){
          if(!inMap(x-1,y)){
              printCM();
              return false;
          }
           x-=1;
       }
       lastDirection = direction;
       if(isBoom()){
           System.out.println("Bomb found!!!");
       }
        return true;
    }
    public boolean walk (char direction, int step){
        int i=0;
        while(i<step){
            if(!walk(direction)){
                return false;
            }
            i++;
    }
        return true;
    }
    public boolean walk(){
        
        return walk(lastDirection);
    }
    public boolean walk(int step){
        return walk(lastDirection,step);
    }
    
    public String toString (){
        return "Robot ("+this.x+","+this.y+")";
    }
    public boolean inMap(int x, int y){
         if(x>=N || x<0 || y-1>=N || y-1<0){
              return false;
          }
        return true;
    }
    public boolean isBoom(){
       if(x==bx && y==by){
           return true;
       }
        return false;
    }
    public void printCM(){
        System.out.println("I can't move!!!");
    }
}

